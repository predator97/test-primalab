import { createRouter, createWebHashHistory, RouterView } from 'vue-router'

import ImageCollection from '../components/Image/collection/collection.vue'
import Login from "../components/Login/Login.vue"
import store from '../store'

const routes = [
  {
    path: '/',
    component: RouterView,
    children:[
      {
        path: '',
        name: "Home",
        redirect: {name: 'ImageCollection'}
      },
      {
        path: 'login',
        name: "Login",
        component: Login,
        meta: { guest: true, title: 'Login Auth' },
      },
      {
        path: 'images',
        meta:{
          title: 'Image'
        },
        component: RouterView,
        children:[
          {
            path: '',
            name: 'ImageCollection',
            component: ImageCollection,
            meta: {requiresAuth: true, title: 'Image collection'},
          },
        ]
      },
    ],
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

const default_title = process.env.VUE_APP_TITLE;

router.afterEach((to) => {
  let new_title = to.meta.title || ''
  document.title = new_title + ' | ' + default_title;
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.getters.isAuthenticated) {
      next();
      return;
    }
    next("/login");
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.guest)) {
    if (store.getters.isAuthenticated) {
      next("/images");
      return;
    }
    next();
  } else {
    next();
  }
});

export default router
