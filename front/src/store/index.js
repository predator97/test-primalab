import { createStore } from 'vuex'
import api from './api'

export default createStore({
  state: {
    images:[],
    image: {},
  },
  mutations: {
    setImages:(state, images)=>state.images = images,
    setImage:(state, image)=>state.image = image,
  },
  actions: {
    login: async({commit},user) => {
      return api.post("login", user)
      .then(rs=>{
        localStorage.setItem('token',rs.data.token)
        commit('setImage',{})
      })
    },
    logout: async({commit}) => {
      localStorage.clear()
      commit('setImages', [])
      commit('setImage', {})
    },
    allImage: async({commit})=>{
      api.get('images')
      .then(rs=>{
        commit('setImages', rs.data)
      })
    },
    create: async({dispatch}, image)=>{

      api.post('images', image)
      .then(()=>{
        console.log("ok suces")
        dispatch('allImage')
      })
      .catch((err)=>console.log(err.message))
    },
    update: async({dispatch}, image)=>{

      api.put('images/'+image.id, image)
      .then(()=>{
        console.log("ok suces")
        dispatch('allImage')
      })
      .catch((err)=>console.log(err.message))
    },
    read: async({commit},id)=>{
      api.get('images/'+id)
      .then(rs=> rs.data)
      .then(rs=> commit('setImage', rs))
    },
    delete: async({dispatch},id)=>{
      await (await api.delete('images/'+id)).data
      dispatch('allImage')
    },
  },
  getters:{
    images:(state)=> state.images,
    image:(state)=> state.image,
    isAuthenticated: ()=> ( localStorage.getItem('token')? true : false )
  },
  modules: {
  }
})
