import axios from 'axios'
import store from './index'

axios.defaults.baseURL = process.env.VUE_APP_API_URL
axios.defaults.headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json; charset=utf-8',
  'Access-Control-Allow-Origin': true,
  'token': localStorage.getItem('token') || ''
}

axios.interceptors.request.use(
  async (config) => {
    config.headers.token = localStorage.getItem('token')
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if(error.response.status == 401)
      store.dispatch('logout')

    return error;
  }
);

export default axios