import { mapActions } from "vuex"

export default{
  name: 'Login',
  data(){
    return{
      user: {
        email: '',
        password: '',
      }
    }
  },
  methods:{
    ...mapActions({
      login: 'login',
    }),
    async loginUser(){
      try{
        await this.login(this.user)
        this.$router.push({name:'ImageCollection'})
      }catch(err){
        console.log(err)
      }
    }
  }
}