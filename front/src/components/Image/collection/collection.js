import { mapActions, mapGetters, mapMutations } from "vuex"

export default{
  data(){
    return {
      url: '',
      file: {},
      screen: false,
    }
  },
  computed:{
    ...mapGetters({
      images: 'images',
      image: 'image',
    }),
  },
  methods:{

    ...mapActions({
      getImages: 'allImage',
      removeImage: 'delete',
      storeImage: 'create',
      updateImage: 'update',
      findImage: 'read'
    }),
    ...mapMutations({
      setImage: 'setImage',
    }),

    addImage(e){
      //conversion diu fichier en base64
      const reader = new FileReader()
      reader.onloadend = () => {
        let a = e.target.files[0]
        this.file.data = reader.result;
        this.file.name = a.name
        this.file.size = a.size
        this.file.type = a.type
      }
      reader.readAsDataURL(e.target.files[0]);

    },

    //suppression de l'image preview
    deleteImage(){
      this.url=""
    },

    //mise a jour de l'image
    async editImage(){
      
      this.updateImage(this.file)
      this.file = {}
    },

    //creation d'une image
    async saveImage(){

      this.storeImage(this.file)
      this.file = {}
    },

    //gestion de l'image
    async handleImage(){
      this.file.id = this.image.id
      
      let img = new Image()
      img.src = this.file.data

      this.file.width = img.width
      this.file.height = img.height

      if(!this.image.id)
        await this.saveImage()
      else
        await this.editImage()
    },

    //recuperation de l'image
    async getImage(id){
      this.screen = false
      await this.findImage(id)
    },

    //preparation a la creation de l'image
    async createImage(){
      this.screen = false
      this.file = {}
      this.url = ''
      this.setImage({})
    },

    //visualisation de l'image
    async viewImage(id){
      await this.getImage(id)
      this.screen = true
    }

  },
  created(){
    this.getImages()
  }
}