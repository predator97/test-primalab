# front

## Project setup
Installer nodejs sur votre poste

executer la commande

```
cp .env.example .env
```
et configurer avec l'adresse du back-end

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
une fois l'application lancée, vous pourrez vous connecter avec le compte suivant:
email: test@test.fr
password: test@prima


### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
