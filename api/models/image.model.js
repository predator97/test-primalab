class Image{
  constructor(
    name,
    width,
    height,
    description
  ){
    this.name = name
    this.width = width
    this.height = height
    this.description = description
  }

}

module.exports = Image