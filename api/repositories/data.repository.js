const fs = require('fs');
const db = 'database/db.json'

class FileRepository{
  
  constructor(){
    this.file = db
  }

  // recuperation de nos données
  async #read(){
    return JSON.parse(fs.readFileSync(this.file).toString())
  }
  //enregistrement de nos données
  async #write(data){

    return fs.writeFile(this.file, JSON.stringify(data), (err) => {
      if (err) throw err
      return true
    })

  }

  //recuperation de toutes les données
  async all(){
    return await this.#read()
  }

  //creation d'un nouvelle données
  async create(newData){

    newData.id = new Date().getTime();

    let _data = await this.#read()
    _data.push(newData)

    return await this.#write(_data)

  }

  //recuperation d'une donnée
  async find(id){
    let output = await this.#read()
    return output.find(out=>out.id == id)
  }

  //mise a jour d'une donnée
  async update(newData){

    let items = await this.#read()
    
    let pos = items.findIndex(item=>item.id == newData.id)

    items[pos] = newData

    return await this.#write(items)

  }

  //suppression d'une donnée
  async delete(id){
    let output = await this.#read()
    output = output.filter(out=>out.id != id)

    return await this.#write(output)
  }

}

module.exports = FileRepository