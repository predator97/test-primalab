const jwt = require('jsonwebtoken');
 
module.exports = (req, res, next) => {

  //verifie la validité du token
  const authHeader = req.headers.token
  jwt.verify(authHeader, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.sendStatus(401);
    }
    req.user = user;
    next();
  });
};