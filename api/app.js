require('dotenv').config();

const express = require('express')
const cors = require("cors")
const app = express()
const Image = require('./models/image.model')
// const multer  = require('multer');
const FileRepository = require('./repositories/data.repository');

const bcrypt = require('bcrypt')
const user = require('./models/user')
const salt = 10
const jwt = require('jsonwebtoken');
const authMiddleware = require('./middleware/auth.middleware')

const port = process.env.port || 3000


// const upload = multer({dest:'uploads/'});

const fileRepository = new FileRepository()

//recuperation de la clé secret
const authTok = process.env.ACCESS_TOKEN_SECRET

const hashPasswordAsync = async(password) => {
  return await bcrypt.hash(password, salt);
}

//gestion de la politique cors
app.use(cors({
  credentials: true
}));

app.listen(port, ()=>console.log(`Server running  on port ${port}...`))

app.use(express.json({ limit: "5mb", extended: true, parameterLimit: 50000 }))
app.use(express.urlencoded({ extended: true}))

app.get("/api", async (req, res)=>{

  res.status(200).send("Hello peeps!")
})

app.post('/api/login', async (req, res) => {
  const { email, password } = req.body;

  const hashedPassword = await hashPasswordAsync(password);

  if(email != user.email || !bcrypt.compare(password, hashedPassword))
    return res.status(401).json({ error: 'User or password is incorrect !' });

  res.status(200).json({
    email: user.email,
    token: jwt.sign(
      { email: user.email },
      authTok,
      { expiresIn: '4h' }
    )
  });
})

app.get('/api/images', authMiddleware, async (req, res) => {

  let result = await fileRepository.all()

  // result.map(rs=>rs.data = "")

  res.status(200).send(result)
})

app.post("/api/images", async (req, res)=>{
  if (!req.body.data) {
    console.log("No file received");
    return res.status(500).send({
      error: "No file received"
    });

  }
  let output  = new Image()

  output.name = req.body.name,
  output.width = req.body.width,
  output.height = req.body.height,
  output.size = req.body.size,
  output.type = req.body.type,
  output.data = req.body.data

  console.log('file received');

  let rest = await fileRepository.create(output)

  return res.status(200).send({
    success: true
  })

})

app.get('/api/images/:id', authMiddleware, async (req, res) => {

  let result = await fileRepository.find(req.params.id)

  if(!result) return res.status(404).send("not found")

  res.send(result)
})

app.put('/api/images/:id', authMiddleware, async (req, res) => {

  let result = await fileRepository.find(req.params.id)
  if(!result) return res.status(404).send("not found")

  if (!req.body.data) {
    console.log("No file received");
    return res.status(500).send({
      error: "No file received"
    });

  }
  let output  = new Image()

  output.id = req.body.id,
  output.name = req.body.name,
  output.width = req.body.width,
  output.height = req.body.height,
  output.size = req.body.size,
  output.type = req.body.type,
  output.data = req.body.data

  console.log('file received');

  await fileRepository.update(output)

  return res.status(200).send({
    success: true
  })
})

app.delete('/api/images/:id', authMiddleware, async (req, res) => {
  let result = await fileRepository.find(req.params.id)
  
  if(!result) return res.status(404).send("not found")
  
  result = await fileRepository.delete(req.params.id)

  res.status(200).send(result)
})
